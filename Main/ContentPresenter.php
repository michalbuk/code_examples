<?php

/**
 * This file is part of the StormAdmin CMS
 *
 * Copyright (c) 2013 STORMSOFT (http://www.stormsoft.cz)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Storm\CMS\AdminModule;

use Model,
        Nette,
        Nette\Application AS NA;

/**
 * Content presenter
 *
 * @author 	Michal Buk
 * @package	Storm
 */
class ContentPresenter extends BasePresenter {

    /** @var Model\Category @inject */
    public $categoryModel;

    /** @var Model\Page @inject */
    public $pageModel;

    /** @var ICategoryGridFactory @inject */
    public $categoryGrid;

    /** @var IPageGridFactory @inject */
    public $pageGrid;

    /** @var ICategoryFormFactory @inject */
    public $categoryForm;

    /** @var IPageFormFactory @inject */
    public $pageForm;

    /**
     * @return CategoryGrid
     */
    protected function createComponentCategoryGrid() {
        return $this->categoryGrid->create();
    }

    /**
     * @return PageGrid
     */
    protected function createComponentPageGrid() {
        return $this->pageGrid->create();
    }

    /**
     * @return CategoryForm
     */
    protected function createComponentCategoryForm() {
        $form = $this->categoryForm->create();
        return $form;
    }

    /**
     * @param int $id
     */
    function actionProcessCategory($id = null) {
        $form = $this['categoryForm'];

        if ($id) {
            $row = $this->categoryModel->find($id);
            if (!$row) {
                throw new NA\BadRequestException('Item not found.');
            }
            $form->setDefaults(array(
                'structure' => $this->structureModel->getDefaultValues($row['structure_id']),
                'category' => $row
                ));
        }
        else {
            if($parent = $this->getParam('parent_id')) {
                $form['category']->setDefaults(array(
                    'parent_id' => $parent,
                    ));
            }
        }
    }

    /**
     * @return PageForm
     */
    protected function createComponentPageForm() {
        $form = $this->pageForm->create();
        return $form;
    }

    /**
     * @param int $id
     */
    function actionProcessPage($id = null) {
        $form = $this['pageForm'];

        if($module = $this->structureModel->getModule()->where('key_name', 'Front')->fetch()) {
            $form['structure']['module_id']->setValue($module['id']);
        }
        if($resource = $this->structureModel->getResource()->where('key_name', 'Content')->fetch()) {
            $form['structure']['resource_id']->setValue($resource['id']);
        }

        if ($id) {
            $row = $this->pageModel->find($id);
            if (!$row) {
                throw new NA\BadRequestException('Item not found.');
            }
            $form->setDefaults(array(
                'structure' => $this->structureModel->getDefaultValues($row['structure_id']),
                'page' => $row
                ));
        }
        else {
            if($action = $this->structureModel->getAction()->where('key_name', 'page')->fetch()) {
                $form['structure']['action_id']->setValue($action['id']);
            }
        }
    }

    function renderProcessPage($id = 0) {
        $row = $this->pageModel->find($id);
        if($row) {
            $this->template->rowTitle = $row->structure['title'];
        }
    }

    function renderProcessCategory($id = 0) {
        $row = $this->categoryModel->find($id);
        if($row) {
            $this->template->rowTitle = $row->structure['title'];
        }
    }

    public function renderPages() {
        $this->addLink('Add page', $this->link('processPage'), 'glyphicon glyphicon-plus');
    }

    public function renderCategories() {
        $this->addLink('Add category', $this->link('processCategory'), 'glyphicon glyphicon-plus');
    }

}