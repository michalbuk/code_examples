<?php

/**
 * This file is part of the StormAdmin CMS
 *
 * Copyright (c) 2012 STORMSOFT (http://www.stormsoft.cz)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Model;

use Storm\Database\NotORM\Model;

/**
 * Category model
 *
 * @author 	STORMSOFT
 * @package	Storm
 */
class Category extends Model {
    
    const KEY_NAME = 'category';
    
    /** @var \Model\Structure @inject */
    public $structureModel;
    
    public function getCategory() {
        return $this->db->category();
    }
    
    public function getAll() {
        return $this->findAll()->where('structure.status', 1)->order('structure.sort ASC');
    }
    
    public function getOne($id) {
        return $this->getAll()->where('id', $id)->fetch();
    }
    
    public function getCategories($group = null, $model = null) {
        $res = $this->getAll();
        if($group) {
            $res->where('structure.model_group.key_name',$group);
        }
        if($model) {
            $res->where('category.model.key_name',$model);
        }
        return $res;
    }
    
    public function getParentTree($parentId, &$arr, &$source) {
        if (isset($source[$parentId])) {
            foreach ($source[$parentId] as $key => $val) {
                $arr[$key] = $val;
                $this->getParentTree($key, $arr[$key]['children'], $source);
            }
        }
    }
    
    public function getTree() {
        $source = array();
        $query = $this->getCategory();
        foreach($query as $row) {
            $source[$row->structure['parent_id']][$row['id']] = array('id' => $row['id'],'title' => $row->structure['title']);
        }
        $res = array();
        $this->getParentTree(NULL, $res, $source);
        return $res;
    }
    
    /**
     * Deletes all associated objects
     * @param int|array $ids
     */
    public function delete($ids) {
        if(!is_array($ids)) {
            $ids = array($ids);
        }
        foreach($ids as $id) {
            $item = $this->find($id);
            // children
            foreach($this->findAll()->where('structure.parent_id', $id) as $row) {
                $row->structure->update(array('parent_id' => $row['parent_id']));
            }
            // structure
            $this->structureModel->deleteStructure($item['structure_id']);
            // item
            $item->delete();
        }
    }
    
}