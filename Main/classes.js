/**
 * StormAdmin JS classes
 * 
 * @copyright Copyright (c) 2013 STORMSOFT.cz
 *
 */

STMA = {
    }

STMA.AdminUserBox = {
    // triggers on userBar hover
    activated: false,
    bar: null,
    barExtended: null,
    timer: null,
    show: function(o) {
        
        if (!this.activated) {
            d = document.getElementById('userBarExtended');
            
            $('#userBar').bind('mouseleave', function(event){
                STMA.AdminUserBox.hide(event);
                event.stopPropagation();
            });
            
            this.bar = o;
            this.barExtended = d;
            this.activated = true;
        } else {
            d = this.barExtended;
        }
        o.style.background = "url('/images/userBarBg.png')";
        d.style.display = "block";
        
    },
    // triggers on extended userBar mouseout
    hide: function(e) {
        var o = STMA.AdminUserBox;
        
        o.barExtended.style.display = "none";
        o.bar.style.background = "";        
    }
}

STMA.Form = {
    syncInputs: function(o, id, callback) {
        var str = o.value;
        if (callback != null) {
            str = str[callback]();
        }
        $('#'+id).val(str);
    },
    hightlightSel: function(o) {
        var str = o.innerHTML;
        if(str.search('---') == -1) {
            o.setAttribute('style', 'font-weight: bold');
        }        
    }
}

STMA.Toggle = {
    single: function(o, id) { 
        
        var src = $(o).find('.expandIcon').attr('src');
        var set = src.search('iconMinus');
        if (set == -1) {
            set = false;
        }
        
        if (set) {
            $(o).find('.expandIcon').attr('src', src.replace('iconMinus', 'iconPlus'));
        } else {
            $(o).find('.expandIcon').attr('src', src.replace('iconPlus', 'iconMinus'));
        }
        
        var cl = o.parentNode.className;
        var regexp = /item-([0-9]+)/g;
        var match, a = 0;
        while ((match = regexp.exec(cl)) != null) {
            if (match[1] != 0) {
                a++;
            }
        }
        
        
        $('.'+id).each(function(){
            var res = $(this).attr('class');
            var skip = false;

            var match, i = 0;
            var regexp = /item-([0-9]+)/g;
            while ((match = regexp.exec(res)) != null) {
                if (match[1] != 0) {
                    i++;
                }
            }
            if (set) {
                var icon = $(this).find('.expandIcon');
                if (icon.length != 0) {
                    var src = icon.attr('src');
                    icon.attr('src', src.replace('iconMinus', 'iconPlus'));                        
                }
                $(this).hide();
            } else {
                if (a == i - 1) {
                    // Direct children
                    $(this).show();
                }
                
            }
    

        });
    },
    multi: function(o, id) {
        var src = $(o).attr('src');
        var show = true;
        var srcSet = src.replace('iconMinus', 'iconPlus');
        if (src.search('iconMinus') == -1) {
            show = false;
            srcSet = src.replace('iconPlus', 'iconMinus');
        }
        
        var tbl = o.parentNode.parentNode.parentNode.parentNode.parentNode;

        $(tbl).find('.expandIcon').attr('src', srcSet);
        $(o).attr('src', srcSet);
        
        var rows = $(tbl).find('tr').not('.item-0').not('.header');
        if (show) {
            rows.hide();
        } else {
            rows.show();
        }
    }
}

STMA.Tabs = {
    swap: function(c, id) {
        var nd = $('#'+c);
        var items = nd.children();
        var input = items[0];
        items[0] = null;
        input.value = id;
        var it = null;
        for(i=0;i<items.length;i++) {
            it = $(items[i]);
            if (it.attr('id') == c +'-nav') {
                // navigation related code
                var children = it.children();
                var navit = null;
                for (a=0;a<children.length;a++) {
                    navit = $(children[a]);
                    if (navit.hasClass('tabs-selected')) {
                        navit.removeClass('tabs-selected');
                    }                  
                }
                var act = $('#'+c+'-nav-'+id);
                if (!act.hasClass('tabs-selected')) {
                    act.addClass('tabs-selected');
                }       
                
            } else {
                // content related code
                if (!it.hasClass('tabs-hide') && it.attr('id') != c + '-' + id) {
                    it.addClass('tabs-hide');
                }
                
            }
        }
        $('#'+c+'-'+id).removeClass('tabs-hide');        
    }
}

STMA.dynamicRedir = function(href, lang) {
    this.dataReturned = false;
    this.dataProcessed = false;
    this.fadeOutCompleted = false;
    // Scope fix
    var sc = this;
    
    if ($.active) return;
    
    // TODO start fadeOut BEFORE the AJAX -> will run more smoothly
    var hgt = $('#contentWrap').height() + parseInt($('#galleryBox').css('margin-top')) + parseInt($('#contentWrap').css('padding-bottom'));
    $('#contentWrap').fadeOut(300, function(){
        sc.fadeOutCompleted = true;
        $('#footerWrap').css({
            'margin-top': hgt+'px'
        });
        // AJAX was faster
        if (!sc.dataProcessed && sc.dataReturned) {
            sc.finish();
        }
    });

    $.ajax({
        url: '/'+href,
        data: 'lang='+lang,
        success: function(data){
            sc.dataReturned = data;
            // Fadeout was faster
            if (sc.fadeOutCompleted) {
                sc.finish();
            }
        },
        dataType: 'html'
    });

    this.finish = function() {
        STMA.hasOwnImages = 1;
        
        var temp = $(this.dataReturned);
        var scr = temp.find("#dynamicVals").html();

        $('#contentWrap').html(temp.find('#contentWrap').html()).hide();

        $('#footerWrap').css({
            'margin-top': '0px'
        });        
        
        // Re-initialize gallery
        nastaveniPozadiByloZavolano = 0;
        if (scr) {
            eval(scr);
        }
        
        
        Cufon.refresh();
        $('#contentWrap').fadeIn(500);
        
        reInitContent();
        
        this.dataProcessed = true;
    }
}

String.prototype.toUrlName = function() {
    var res = this;
    
    var item = null;
    var dels = new Array('+',';',':','/','?','!','.',',','"','\'','#','&','^','%','@','(',')','[',']','{','}');
    res = res.replaceAll(/\\/g, '');
    res = res.replaceAll(/ /g, '-');
    res = res.toLowerCase();
    var toReplace = new Array('=','ě','Ě','š','Š','č','Č','ř','Ř','ž','Ž','ý','Ý','á','Á','í','Í','é','É','ú','Ú','ů','Ů','ť','Ť','ď','Ď','ó','Ó','ň','Ň','ä','Ä','ë','Ë','ü','Ü','ö','Ö');
    var replaces = new Array('-','e','E','s','S','c','C','r','R','z','Z','y','Y','a','A','i','I','e','E','u','U','u','U','t','T','d','D','o','O','n','N','a','A','e','E','u','U','o','O');
    var i;
    var rep;
    for(i=0;i<this.length;i++) {
        rep = null;
        item = this.substr(i, 1);
        if (dels.search(item) != -1) {
            rep = '';
        }
        var r = toReplace.search(item);
        if (r != -1) {
            rep = replaces[r];
            
        }
        if (rep !== null) {
            res = res.replace(item, rep);
        }
    }
    return res;
}

String.prototype.replaceAll = function(replace, _with) {
    return this.replace(new RegExp(replace),_with);  
}