<?php

/**
 * This file is part of the StormAdmin CMS
 *
 * Copyright (c) 2012 STORMSOFT (http://www.stormsoft.cz)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Storm\CMS\AdminModule;

use Model,
        Nette,
        Nette\Forms\Container,
        Nette\Utils as NU,
        Storm\Application\UI\Form;

/**
 * Role form
 *
 * @author 	STORMSOFT
 * @package	Storm
 */
interface IRoleFormFactory {

    /** @return RoleForm */
    function create();
}

class RoleForm extends Form {

    /** @var Model\User */
    private $userModel;

    /**
     *
     * @param Model\User $userModel
     */
    public function __construct(Model\User $userModel) {
        parent::__construct();
        $this->userModel = $userModel;
    }

    protected function configure() {

        $this->getElementPrototype()->class('form-horizontal form-edit');

        $this->addText('name', 'Name:')
                ->setRequired();
        $this->addText('key_name', 'Key name:')
                ->setRequired();
        $this->addSelect('parent_id', 'Parent:', \Storm\Utils\Arrays::tree($this->structureModel->getRolesTree(),false,'id','key_name','children'), 8)
                ->setPrompt('- root -')
                ->setTranslator(null);
        $this->addTextArea('comment', 'Comment:');

        $this->addSubmit("save", "Save")->onClick[] = callback($this, "saveClicked");
        $this['save']->getControlPrototype()->class('btn btn-primary');

        $this->addSubmit("close", "Save And Close")->onClick[] = callback($this, "saveClicked");
        $this['close']->getControlPrototype()->class('btn btn-success');

        $this->addSubmit("cancel", "Cancel")->setValidationScope(FALSE)->onClick[] = callback($this, "cancelClicked");
        $this['cancel']->getControlPrototype()->class('btn btn-link');
    }

    public function saveClicked($button) {
        $form = $button->form;
        $values = $form->getValues(true);

        $id = $this->presenter->getParam('id');

        if ($id) {
            // update
            $this->presenter->flashMessage("Item was updated.", "alert-success");
        } else {
            // insert
            $this->presenter->flashMessage("Item was created.", "alert-success");
        }

        try {
            $return = $this->structureModel->getRole()->insert_update(array('id' => $id), $values, $values);
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
            return;
        }

        if ($form->valid) {
            if ($button->name == 'save') {
                $this->presenter->redirect('this', $return);
            } else {
                $this->presenter->redirect('roles');
            }
        }
    }

    public function cancelClicked($button) {
        $this->presenter->flashMessage("Changes have been canceled.", "alert-info");
        $this->presenter->redirect('roles');
    }

}
