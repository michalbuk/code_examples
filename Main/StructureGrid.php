<?php

/**
 * This file is part of the StormAdmin CMS
 *
 * Copyright (c) 2013 STORMSOFT (http://www.stormsoft.cz)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

namespace Storm\CMS\AdminModule;

use Storm\Addons\Grid;

/**
 * Content
 *
 * @author 	Michal Buk
 * @package	Storm
 */
interface IStructureGridFactory {

    /** @return StructureGrid */
    function create();
}
class StructureGrid extends Grid {

    /** @var \Model\Structure */
    private $structureModel;

    /** @var \GettextTranslator\Gettext */
    protected $translator;

    /** @var int */
    protected $structureId;

    /**
     *
     * @param \Model\Structure $structureModel
     * @param \GettextTranslator\Gettext $translator
     */
    function __construct(\Model\Structure $structureModel, \GettextTranslator\Gettext $translator, $structureId = null) {
        parent::__construct();
        $this->structureModel = $structureModel;
        $this->translator = $translator;
        $this->structureId = $structureId;
    }

    protected function configure($presenter) {
        $res = $this->structureModel->table
                ->where('store IS NOT NULL AND parent_id', $this->structureId);
        $source = new \NiftyGrid\NDataSource($res);

        $this->setDataSource($source);
        $this->setTranslator($this->translator);

        $this->setWidth("100%");
        $this->setDefaultOrder("sort ASC");
        $this->setSortable(TRUE);

        $this->addColumn('link', 'Link', null, 30)
                ->setTextEditable()
                ->setTextFilter()
                ->setAutocomplete(5);

        $this->addColumn('title', 'Title', null, 30)
                ->setTextEditable()
                ->setTextFilter()
                ->setAutocomplete(5);

        $modules = $this->structureModel->module->fetchPairs('id', 'key_name');
        $this->addColumn('module_id', 'Module')
                ->setRenderer(function($row) {return $row->module['key_name'];})
                ->setSelectFilter($modules);

        $this->addColumn('sort', 'Sort')
                ->setTextEditable();

        $translator = $this->translator;
        $this->addColumn('status', 'Status')
                ->setSelectEditable(array(0 => "Disabled", 1 => "Active", 2 => "Hidden"))
                ->setRenderer(function($row) use ($translator) {
                            $el = \Nette\Utils\Html::el('span class=label');
                            if($row['status'] == 1) {
                                $el->setText($translator->translate('Active'))->addClass('label-success');
                            }
                            elseif($row['status'] == 2) {
                                $el->setText($translator->translate('Hidden'))->addClass('label-inverse');
                            }
                            else {
                                $el->setText($translator->translate('Disabled'))->addClass('label-default');
                            }
                            return $el;
                        });


        $self = $this;
        $structure = $this->structureModel;
        $this->setRowFormCallback(function($values) use ($self, $structure) {
                    $vals = array(
                        "id" => $values["id"],
                    );
                    $struct = array(
                        "link" => $values["link"],
                        "title" => $values["title"],
                        "status" => $values["status"],
                    );
                    $row = $structure->getStructure()->where('id', $vals['id'])->fetch();
                    $row->update($struct);
                    $self->flashMessage("Your changes have been saved.", "grid-successful");
                }
        );

        $this->addButton(Grid::ROW_FORM, "Fast edit")
                ->setIcon('glyphicon glyphicon-wrench');

        $this->addButton("edit", "Edit")
                ->setIcon("glyphicon glyphicon-pencil")
                ->setLink(function($row) use ($presenter) {
                            return $presenter->link("process", $row['id']);
                        })
                ->setAjax(FALSE);

        $this->addButton("add", "Add item")
                ->setIcon("glyphicon glyphicon-plus")
                ->setLink(function($row) use ($presenter) {
                            return $presenter->link("process", array('parent_id' => $row['id']));
                        })
                ->setAjax(FALSE);

        $this->addButton("delete", "Delete")
                ->setPermission('deleteStructure')
                ->setClass("delete")
                ->setLink(function($row) use ($self) {
                            return $self->link("deleteStructure!", $row['id']);
                        })
                ->setConfirmationDialog("Are you sure you want to proceed?");

        $this->addAction("disable", "Disable selected")
                ->setCallback(function($id) use ($self) {
                            return $self->handleDisable($id);
                        });

        $this->addAction("delete", "Delete selected")
                ->setPermission('deleteStructure')
                ->setCallback(function($id) use ($self) {
                            return $self->handleDeleteStructure($id);
                        })
                ->setConfirmationDialog("Are you sure you want to proceed?");

        $this->addSubGrid("structure", "Show attached structures")
                ->setGrid(new StructureGrid($this->structureModel, $this->translator, $this->activeSubGridId))
                ->settings(function($grid) {
                            $grid->setWidth("100%;");
                        })
                ->setCellStyle("background-color:#f6f6f6; padding:20px;");
    }

    public function handleDeleteStructure($id) {
        $res = $this->structureModel->findAll()->where("id", $id);
        $size = count($res);

        // Set children's parent_id
        foreach($res as $row) {
            $children = $this->structureModel->findAll()->where('parent_id', $row['id']);
            foreach($children as $item) {
                $item->update(array('parent_id' => $row['parent_id']));
            }
        }
        $res->delete();
        $this->flashMessage(($size>1 ? "Items" : "Item") . " has been deleted.", "alert-info");
    }

    public function handleDisable($id) {
        $res = $this->structureModel->findAll()->where("id", $id);
        $size = count($res);

        foreach($res as $row) {
            $row->update(array("status" => 0));
        }

        $this->flashMessage(($size>1 ? "Items" : "Item") . " has been disabled.", "alert-info");
    }


}
